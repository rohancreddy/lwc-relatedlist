# RelatedList Inline Edit Component

There are 4 components to this : 
1. RelatedListController
2. RelatedListControllerTest
3. relatedList LWC
4. Permission set - relatedListClassAccess

# How to deploy
1. Clone the repository via VS code
2. Connect to a dev/sandbox org and deploy the package.xml to the org.
3. To deploy in production environment, use any method you would normally use to deploy components (change set/clickdeploy.io etc)

# How to use
1. Add the custom lightning component "Dynamic Related List" to any lightning record page.
2. There are 5 input parameters.
3. Below is an example if we place the component on the Account detail page and wish to show Contacts : 
	  1. API Name of the related object : Contact
	  2. API Name of the relation field to the parent object : AccountId
	  3. API Name of the relation field on this object to the parent object : Id
	  4. Comma-delimited list of fields to show : FirstName, LastName, Email
	  5. Condition to filter records : Title = 'CEO'
4. Below is an example if we place the component on a CustomObject__c and wish to show Contacts related to a common Account :
    1. API Name of the related object : Contact
    2. API Name of the relation field to the parent object : AccountId
    3. API Name of the relation field on this object to the parent object : Account__c
    4. Comma-delimited list of fields to show : Name, Email, Phone
    4. Condition to filter records : Title = 'CEO'

5. Assign the permission set "relatedListClassAccess" to all users who need access to the component
