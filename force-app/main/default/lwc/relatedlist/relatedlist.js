import { LightningElement, wire, track, api } from "lwc";
import getRecordsDynamically from "@salesforce/apex/RelatedListController.getRecordsDynamically";
import { NavigationMixin } from "lightning/navigation";
import { deleteRecord } from "lightning/uiRecordApi";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { getObjectInfo } from "lightning/uiObjectInfoApi";

export default class Relatedlist extends NavigationMixin(LightningElement) {
  @api columnFields;
  @api relatedListTitle;
  @api relationField;
  @api recordRelation;
  @api recordId;
  @api objectApiName;
  @api whereCondition;
  @track headerColumns;
  @track editableColumns;
  @track columnData;
  @track deletePrompt = false;
  @track deleteRecordId;
  @track showCancelBtn;
  @track showUpdateBtn;

  @track currentPage = 1;
  @track noOfPages = 0;
  @track pageSize = 10;
  @track currentPageData;
  @track paging = {
    disableFirst: false,
    disablePrev: false,
    disableNext: false,
    disableLast: false
  };

  @api objectName;
  _objInfo;
  _orderByField;
  _orderByType = "ASC";

  _formsToUpdate = [];
  _formsToInsert = [];
  _isDelete;

  
  /**
   * Retrieves parent record
   */
  @wire(getObjectInfo, { objectApiName: "$objectName" })
  wiredRelObjInfo({ error, data }) {
    if (data) {
      this._objInfo = data;
      this.initializeTable();
    } else if (error) {
      this.showNotification("Error getting object information", error, "error");
      this._objInfo = undefined;
    }
  }

  reloadData() {
    this.fetchData();
  }
  get renderTable() {
    return this.currentPageData && this.currentPageData.length > 0;
  }
  _newRecordKey = 1000;
  /**
   * Loads a subset of array records into current page
   * @param {array} array - List of all Split records
   * @param {integer} page_size - Number of records to show on list
   * @param {integer} page_number - Page number to flip to
   */
  paginate(array, page_size, page_number) {
    this.paging.disableFirst = false;
    this.paging.disablePrev = false;
    this.paging.disableNext = false;
    this.paging.disableLast = false;
    if (page_number === 1 || this.noOfPages === 1) {
      this.paging.disableFirst = true;
      this.paging.disablePrev = true;
    }
    if (page_number === this.noOfPages || this.noOfPages === 1) {
      this.paging.disableNext = true;
      this.paging.disableLast = true;
    }
    --page_number;

    this.currentPageData = array.slice(
      page_number * page_size,
      (page_number + 1) * page_size
    );
    let rIdx = 0;
    this.currentPageData.forEach(d => {
      let fIdx = 0;
      d.recData.forEach(f => {
        f.key = rIdx + ":" + fIdx++;
      });
      d.Idx = rIdx++;
      if (!d.key) d.key = d.Id ? d.Id : this._newRecordKey++;
    });
  }

  /**
   * Handles pagination request based on the control button that was clicked
   * @param {object} event - Event for pressed button (contains action in label)
   */
  doPagination(event) {
    const act = event.target.label;
    if (act === "First") {
      this.currentPage = 1;
    } else if (act === "Prev") {
      this.currentPage--;
    } else if (act === "Next") {
      this.currentPage++;
    } else if (act === "Last") {
      this.currentPage = this.noOfPages;
    }
    this.paginate(this.columnData, this.pageSize, this.currentPage);
  }

  get renderPaginationBtns() {
    return this.currentPageData && this.noOfPages && this.noOfPages > 1;
  }

  get currentShowing() {
    return "Page " + this.currentPage + " of " + this.noOfPages;
  }
  _noOfNewRecords = 0;

  /**
   * Adds a new row to the top of the list
   */
  handleNewOption() {
    try {
      let newCol = { recData: [] };
      this.headerColumns.forEach(helement => {
        newCol.recData.push({
          field: helement.fieldName,
          display: true,
          editNow: true,
          editable: this.editableColumns[helement.fieldName]
        });
      });
      this.columnData.splice(0, 0, newCol);
      this._noOfNewRecords++;
      this.currentPage = 1;
      this.noOfPages = Math.ceil(this.columnData.length / this.pageSize);
      this.paginate(this.columnData, this.pageSize, this.currentPage);
      this.showCancelBtn = true;
      this.showSaveOrUpdateBtn = true;
    } catch (error) {
      this.showNotification("Error setting up new record", error, "error");
    }
  }

  get disableNewBtn() {
    return this._noOfNewRecords >= 10;
  }

  /**
   * Queries data with apexParams JSON object as a parameter that carries all the data reuired for creating soql query as key value pairs
   * the returned result would be will be used to construct list of objects on which a pagination will be applied.
   */
  queryForData() {
    let apexParams = {};
    apexParams.objectName = this.objectName;
    apexParams.fields = this.columnFields;
    apexParams.relationField = this.relationField;
    apexParams.recordRelation = this.recordRelation;
    apexParams.objectApiName = this.objectApiName;
    apexParams.parentId = this.recordId;
    apexParams.whereClause = this.whereCondition;
    apexParams.orderField = this._orderByField;
    apexParams.orderType = this._orderByType;
    const headerColumns = this.headerColumns;
    const editableColumns = this.editableColumns;
    getRecordsDynamically(apexParams)
      .then(result => {
        let colData = [];
        result.forEach(function(element) {
          let output = [];
          output.push({
            value: element.Id,
            field: "Id",
            display: false, //when false will not be displayed on the UI
            editNow: false //when true the corresponding field will be in edit mode on UI
          });
          headerColumns.forEach(function(helement) {
            output.push({
              value: element[helement.fieldName],
              field: helement.fieldName,
              display: true,
              editNow: false,
              editable: editableColumns[helement.fieldName]
            });
          });
          colData.push({
            Id: output[0].value,
            recData: output
          });
        });
        this.columnData = colData;
        this.currentPage = 1;
        this.noOfPages = Math.ceil(this.columnData.length / this.pageSize);
        this.paginate(this.columnData, this.pageSize, this.currentPage);
      })
      .catch(error => {
        this.showNotification("Error getting records", error, "error");
      });
  }

  /**
   * Handles sort request based on the control button that was clicked
   * @param {object} event - Event for pressed button (contains action in label)
   */
  doSorting(event) {
    const chosenSortField = event.target.value;
    if (this._orderByField === chosenSortField) {
      this._orderByType = this._orderByType === "ASC" ? "DESC" : "ASC";
    } else {
      this._orderByType = "ASC";
    }
    this.headerColumns.forEach(hc => {
      if (this._orderByField === chosenSortField) {
        if (hc.fieldName === chosenSortField) {
          hc.sortIcon =
            this._orderByType === "ASC"
              ? "utility:arrowup"
              : "utility:arrowdown";
          hc.sortClass = "currentSorted";
        }
      } else {
        if (hc.fieldName === chosenSortField) {
          hc.sortIcon =
            this._orderByType === "ASC"
              ? "utility:arrowup"
              : "utility:arrowdown";
          hc.sortClass = "currentSorted";
        } else {
          hc.sortIcon = "utility:sort";
          hc.sortClass = "headerBtn lightIcon";
        }
      }
    });
    this._orderByField = chosenSortField;
    this.queryForData();
  }

  /**
   * Loads column data based on "columnFields" that were provided by the user
   */
  initializeTable() {
    if (
      !this.recordId ||
      !this.objectName ||
      !this.relationField ||
      !this.columnFields ||
      !this._objInfo
    )
      return;

    this.resetDmlBtns();
    let tableHeaderInfo = [];
    let editableFields = {};
    this.columnFields = this.cleanFields(this.columnFields);
    if (this._objInfo.nameFields && this._objInfo.nameFields[0])
      this._orderByField = this._objInfo.nameFields[0];

    const colFieldsArray = this.columnFields.split(",");
    colFieldsArray.forEach(field => {
      field = field.trim();
      tableHeaderInfo.push({
        fieldName: field,
        label: this._objInfo.fields[field].label,
        sortable: this._objInfo.fields[field].sortable,
        sortIcon:
          field === this._orderByField ? "utility:arrowup" : "utility:sort",
        sortClass:
          field === this._orderByField ? "currentSorted" : "headerBtn lightIcon"
      });
      editableFields[field] = this._objInfo.fields[field].updateable;
    });
    this.headerColumns = tableHeaderInfo;
    this.editableColumns = editableFields;
    this.queryForData();
  }

  fetchData() {
    this.queryForData();
  }

  renderRow = 0;
  renderCol = -1;

  /**
   * Renders selected row in edit mode and queues corresponding record into _formsToUpdate array
   * @param {object} event - Event for clicked row (contains selected row id)
   */
  goEditing(event) {
    const col_row_id = event.target.value.split(":");
    const editRowIdx = Number(col_row_id[0]);
    this.currentPageData[editRowIdx].recData[
      Number(col_row_id[1])
    ].editNow = true;
    const rId = this.currentPageData[editRowIdx].Id;
    if (!this._formsToUpdate.includes(rId)) this._formsToUpdate.push(rId);

    // toggle save/cancel buttons
    this.showCancelBtn = true;
    this.showSaveOrUpdateBtn = true;
  }

  /**
   * Stores value changes in memory in currentPageData
   * @param {object} event - Event for field that was changed (contains row id)
   */
  handleFieldValueChange(event) {
    if (!event.target.value) return;

    const col_row_id = event.target.dataset.item.split(":");
    const editRowIdx = Number(col_row_id[0]);
    if (!this.currentPageData[editRowIdx].Id) {
      this.currentPageData[editRowIdx].recData[Number(col_row_id[1])].value =
        event.target.value;
    }
  }

  /**
   * Handles action clicks (e.g. view or delete record)
   * @param {object} event - Event for menu action that was clicked (contains action in label)
   */
  menuAction(event) {
    event.preventDefault();
    const act = event.target.label;
    const actId = event.target.value;
    this._isDelete = false;

    if (act === "View") {
      // open up record detail page
      this[NavigationMixin.Navigate]({
        type: "standard__recordPage",
        attributes: {
          recordId: actId,
          actionName: "view"
        }
      });
    } else if (act === "Delete") {
      // remove selected record from list and reset current page to 1
      if (typeof actId === "number" && actId < 10) {
        this.columnData.splice(actId, 1);

        this._noOfNewRecords--;
        this.currentPage = 1;
        this.noOfPages = Math.ceil(this.columnData.length / this.pageSize);
        this.paginate(this.columnData, this.pageSize, this.currentPage);
        if (this._noOfNewRecords === 0 && this._formsToUpdate.length === 0) {
          this.showCancelBtn = false;
          this.showSaveOrUpdateBtn = false;
        }
      } else {
        this._isDelete = true;
        this.deleteRecordId = actId;
        this.deletePrompt = true;
      }
    }
  }

  deleteCancel() {
    this._isDelete = false;
    this.deletePrompt = false;
  }

  /**
   * Handles delete confirmation by deleting record from database
   */
  deleteConfirmed() {
    this.deletePrompt = false;

    deleteRecord(this.deleteRecordId)
      .then(() => {
        this.showNotification("Success", "Record deleted", "success");
        const deleteRecordId = this.deleteRecordId;
        this.columnData = this.columnData.filter(function(obj) {
          return obj.Id !== deleteRecordId;
        });
        this.fetchData();
      })
      .catch(error => {
        this.fetchData();
        this.showNotification(
          "Error deleting record",
          error.body.message,
          "error"
        );
      });
  }

  handleCancel() {
    this._noOfNewRecords = 0;
    this.resetDmlBtns();
    this.fetchData();
  }

  /**
   * Saves records that were stored in _formsToUpdate to database
   */
  handleSave() {
    this._isDelete = false;
    let allValid = true;
    this.template.querySelectorAll("lightning-input-field").forEach(f => {
      const isValid = f.reportValidity();
      if (allValid) allValid = isValid;
    });
    if (allValid) {
      this.template
        .querySelectorAll("lightning-record-edit-form")
        .forEach(e => {
          if (this._formsToUpdate.includes(e.recordId) || !e.recordId)
            e.submit();
        });
    }
  }

  /**
   * Saves records that were stored in _formsToUpdate to database
   */
  handleUpdate() {
    this._isDelete = false;
    this.template.querySelectorAll("lightning-record-edit-form").forEach(e => {
      if (this._formsToUpdate.includes(e.recordId)) e.submit();
    });
  }

  /**
   * Inserts new record into database
   */
  handleInsert() {
    this._isDelete = false;
    let allValid = true;
    this.template.querySelectorAll("lightning-input-field").forEach(f => {
      const isValid = f.reportValidity();
      if (allValid) allValid = isValid;
    });
    if (allValid) {
      this.template
        .querySelectorAll("lightning-record-edit-form")
        .forEach(e => {
          if (!e.recordId) e.submit();
        });
    }
  }

  handleSuccess() {
    this.showNotification("Success", "Saved Successfully", "success");
    this.resetDmlBtns();
    this.fetchData();
  }

  handleError(event) {

    if (
      this._isDelete &&
      event.detail.message === "The requested resource does not exist"
    )
      return;
    if (
      event.detail.output &&
      event.detail.output.errors &&
      Array.isArray(event.detail.output.errors)
    ) {
      const messageToDisplay = event.detail.output.errors.reduce(
        (m, e, i) => m + (i > 0 ? ", " : "") + (i + 1) + ") " + e.message,
        ""
      );
      this.showNotification(
        "Review the errors on this page.",
        messageToDisplay,
        "error",
        "sticky"
      );

    } else {
      this.showNotification(
        "Review the errors on this page.",
        event.detail.message,
        "error",
        "sticky"
      );

    }
  }

  resetDmlBtns() {
    this.showCancelBtn = false;
    this.showSaveOrUpdateBtn = false;
    this.showUpdateBtn = false;
    this.showInsertBtn = false;
  }

  /**
   * Checks if the fields in config are all present on the object and displays a warning message if not
   * @param {array} fields - List of comma delimited field API names
   */
  cleanFields(fields) {
    let unknownFields = [];
    let validFields = [];
    fields.split(",").forEach(f => {
      f = f.trim();
      if (this._objInfo.fields.hasOwnProperty(f)) validFields.push(f);
      else unknownFields.push(f);
    });

    if (unknownFields.length > 0)
      this.showNotification(
        "Unknown fields",
        unknownFields.join(", ") +
          " fields are not valid for the object " +
          this._objInfo.label,
        "warning"
      );
    return validFields.join();
  }

  get objectNameSet() {
    return this._objInfo !== undefined;
  }

  get objPluralLabel() {
    return (
      this._objInfo.labelPlural +
      (this.columnData ? " (" + this.columnData.length + ")" : "")
    );
  }
 
  get ComponentTitleText() {
    if (this.relatedListTitle.length > 0) {
      return this.relatedListTitle;
    }
    return (
      this._objInfo.labelPlural +
      (this.columnData ? " (" + this.columnData.length + ")" : "")
    );
  }

  get objectIconImage() {
    return this._objInfo.themeInfo.iconUrl;
  }
  get objectBackColor() {
    return "background-color: #" + this._objInfo.themeInfo.color;
  }

  showNotification(title, message, variant, mode = "dismissable") {
    const evt = new ShowToastEvent({
      title: title,
      message: message,
      variant: variant,
      mode: mode
    });
    this.dispatchEvent(evt);
  }
}