/**
 *  Controller for providing CRUD operations to the relatedlist component
 */
public with sharing class RelatedListController {/**
     *  Queries the sObject records that is related to a parent record
     *  @param objectName - sObject to be queried
     *  @param fields - fields to be queried
     *  @param relationField - relationship field of this object to it's parent object
     *  @param parentId - the parent record Id
     *  @param whereClause - the condition applied to the records 
     *  @param orderField - the field to be used for ordering the records 
     *  @param orderType - the type of ordering, possible values are ASC and DESC
     *  @return List of related records
     */
    @AuraEnabled 
    public static List<sObject> getRecordsDynamically(String objectName, String fields, String relationField, String recordRelation,String objectApiName, String parentId, String whereClause, String orderField, String orderType){
        
        /* To get parent object id */
        String idQuery = 'SELECT ' + recordRelation + ' FROM ' + objectApiName + ' WHERE Id = \'' + parentId +'\'';
        sObject parentObj = Database.query(idQuery);
        String parentRecordId = String.valueOf(parentObj.get(recordRelation));

        /* To get related records for parent id retrieved above */
        String query = '';
        if(whereClause == Null || whereClause == ''){
            query = 'SELECT ' + fields + 
            ' FROM '+ objectName +
            ' WHERE '+ relationField +' = \''+ parentRecordId +'\' ORDER BY ' + orderField + ' ' + orderType;
        } else {
            query = 'SELECT ' + fields + 
            ' FROM '+ objectName +
            ' WHERE '+ relationField +' = \''+ parentRecordId +'\' AND ' + whereClause + ' ORDER BY ' + orderField + ' ' + orderType;
        }
        return Database.query(query);
    }
}