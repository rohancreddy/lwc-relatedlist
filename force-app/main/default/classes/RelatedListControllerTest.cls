@isTest
private class RelatedListControllerTest 
{
    @isTest
    private static void getRecordsDynamically_returnStringQueryWithoutClause()
    {
        String conId;
        
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'Test Contact';
        c.AccountId = a.Id;
        insert c;
        
        System.Test.startTest();
        conId = RelatedListController.getRecordsDynamically('Contact','Id','AccountId','Id', 'Account',a.Id,Null,'CreatedDate','ASC')[0].Id;    
        System.Test.stopTest();
        
        System.assertEquals(c.Id,conId);
    }

    @isTest
    private static void getRecordsDynamically_returnStringQueryWithClause()
    {
        String conId;
        
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;
        
        Contact c = new Contact();
        c.LastName = 'Test Contact';
        c.AccountId = a.Id;
        insert c;
        
        System.Test.startTest();
        conId = RelatedListController.getRecordsDynamically('Contact','Id','AccountId','Id', 'Account',a.Id,'Name != Null','CreatedDate','ASC')[0].Id;    
        System.Test.stopTest();
        
        System.assertEquals(c.Id,conId);
    }
    
    @isTest
    private static void getRecordsDynamically_returnNullException()
    {
        String message;
        
        Contact c = new Contact();
        c.LastName = 'Test Contact';
        insert c;
        
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;
        
        System.Test.startTest();
        try
        {
            RelatedListController.getRecordsDynamically(null,null,null,null,null,null,null,null,null);
            System.assert(false, 'Expected to receive an exception.');
        }
        catch(Exception e)
        {
            message = e.getMessage();
        }
        System.Test.stopTest();   
        
        System.assertEquals('unexpected token: \'null\'',message);
    }
    
}